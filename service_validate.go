package auth

import (
	"bitbucket.org/gilius/brunas-client-auth/models"
	"bitbucket.org/gilius/go-toolbox"
	"github.com/gorilla/websocket"
	"net/http"
	"sync"
)

type ServiceValidate interface {
	ClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.Claims)) func(http.ResponseWriter, *http.Request)
	UserClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.UserClaims)) func(http.ResponseWriter, *http.Request)
	ServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.ServiceClaims)) func(http.ResponseWriter, *http.Request)

	SuperOrServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims)) func(http.ResponseWriter, *http.Request)
	AnyClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims, *models.UserClaims)) func(http.ResponseWriter, *http.Request)

	OptionalClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.Claims)) func(http.ResponseWriter, *http.Request)
	OptionalUserClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.UserClaims)) func(http.ResponseWriter, *http.Request)
	OptionalServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims)) func(http.ResponseWriter, *http.Request)

	SocketJWTClaimsRequest(handler func(*websocket.Conn, *http.Request, *sync.Mutex, models.Claims)) func(http.ResponseWriter, *http.Request)
	SocketUserJWTClaimsRequest(handler func(*websocket.Conn, *http.Request, *sync.Mutex, models.UserClaims)) func(http.ResponseWriter, *http.Request)
	SocketAnyClaimsRequest(handler func(*websocket.Conn, *http.Request, *sync.Mutex, *models.ServiceClaims, *models.UserClaims)) func(http.ResponseWriter, *http.Request)

	VerifyUserClaims(tokenString string) (*models.UserClaims, *toolbox.Error)
	VerifyServiceClaims(tokenString string) (*models.ServiceClaims, *toolbox.Error)
}
