module bitbucket.org/gilius/brunas-client-auth

go 1.21

require (
	bitbucket.org/gilius/go-toolbox v1.1.22
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/gorilla/websocket v1.5.3
	go.mongodb.org/mongo-driver v1.17.3
)
