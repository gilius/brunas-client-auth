package models

import "go.mongodb.org/mongo-driver/bson/primitive"

type Client struct {
	Id         primitive.ObjectID `json:"id" bson:"_id"`
	Name       string             `json:"name" bson:"name"`
	AppEnabled bool               `json:"appEnabled" bson:"app_enabled"`
	Tags       map[string]string  `json:"data" bson:"data"`
	Domains    []string           `json:"domains" bson:"domains"`
}
