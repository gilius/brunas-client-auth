package auth

import (
	"bitbucket.org/gilius/go-toolbox"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"github.com/golang-jwt/jwt"
	"log"
	"os"
)

var RepoErr = toolbox.ErrorInternal("repository_error")

const keyDir = "./storage/keys/"
const publicKeyLocation = keyDir + "public.key"

type repositoryDisk struct {
	logger *log.Logger
}

// fileExists checks if a file exists and is not a directory
func (r repositoryDisk) fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func (r repositoryDisk) GetPublicKey() (*rsa.PublicKey, *toolbox.Error) {
	if !r.fileExists(publicKeyLocation) {
		return nil, nil
	}

	verifyBytes, err := os.ReadFile(publicKeyLocation)
	if err != nil {
		r.logger.Println("Failed to read public key")
		r.logger.Println(err)
		return nil, RepoErr
	}

	verifyKey, err := jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	if err != nil {
		r.logger.Println("Failed to parse public key")
		r.logger.Println(err)
		return nil, RepoErr
	}
	return verifyKey, nil
}

// PutPublicKey Saves public key
func (r repositoryDisk) PutPublicKey(publicKey rsa.PublicKey) *toolbox.Error {
	asn1Bytes, err := x509.MarshalPKIXPublicKey(&publicKey)
	if err != nil {
		return toolbox.ErrorInternal(err.Error())
	}

	var keyPEM = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: asn1Bytes,
	}

	filePEM, err := os.Create(publicKeyLocation)
	if err != nil {
		return toolbox.ErrorInternal(err.Error())
	}
	defer func() {
		_ = filePEM.Close()
	}()

	err = pem.Encode(filePEM, keyPEM)
	if err != nil {
		return toolbox.ErrorInternal(err.Error())
	}

	return nil
}

//goland:noinspection GoUnusedExportedFunction
func NewRepositoryDisk(logger *log.Logger) KeyRepository {
	if _, err := os.Stat(keyDir); os.IsNotExist(err) {
		err := os.MkdirAll(keyDir, os.ModeDir)
		if err != nil {
			logger.Println("Failed to create directory '" + keyDir + "'")
			os.Exit(1)
		}
	}
	return &repositoryDisk{logger: logger}
}
