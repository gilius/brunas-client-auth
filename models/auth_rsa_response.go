package models

import (
	"bitbucket.org/gilius/go-toolbox"
	"crypto/rsa"
)

type AuthRsaResponse struct {
	toolbox.ResponseObject
	Data rsa.PublicKey `json:"data"`
}
