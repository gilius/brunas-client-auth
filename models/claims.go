package models

import (
	"github.com/golang-jwt/jwt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type Claims struct {
	ClientIdList []primitive.ObjectID `json:"clientIdList"`
	ExpiresAt    int64                `json:"exp,omitempty"`
	jwt.Claims   `json:"-"`
}

func (c Claims) IsExpired() bool {
	return time.Now().After(time.Unix(c.ExpiresAt, 0))
}

func (c Claims) AllowedToAccessClient(clientId primitive.ObjectID) bool {
	if len(c.ClientIdList) == 0 {
		return true
	}
	for _, item := range c.ClientIdList {
		if item.Hex() == clientId.Hex() {
			return true
		}
	}

	return false
}
