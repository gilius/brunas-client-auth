package auth

import (
	"bitbucket.org/gilius/go-toolbox"
	"crypto/rsa"
)

type KeyRepository interface {
	GetPublicKey() (*rsa.PublicKey, *toolbox.Error)
	PutPublicKey(rsa.PublicKey) *toolbox.Error
}
