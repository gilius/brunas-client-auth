package models

import (
	"github.com/golang-jwt/jwt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type UserClaims struct {
	Id             primitive.ObjectID   `json:"id"`
	FirstName      string               `json:"firstName"`
	LastName       string               `json:"lastName"`
	Username       string               `json:"username"`
	Email          string               `json:"email"`
	Phone          string               `json:"phone"`
	Picture        string               `json:"picture"`
	Gender         string               `json:"gender"`
	Roles          []string             `json:"roles"`
	Clients        []Client             `json:"clients"`
	ClientIdList   []primitive.ObjectID `json:"clientIdList"`
	ExpeditionList []primitive.ObjectID `json:"expeditions"`
	ExpiresAt      int64                `json:"exp,omitempty"`
	jwt.Claims     `json:"-"`
}

func (c UserClaims) IsExpired() bool {
	return time.Now().After(time.Unix(c.ExpiresAt, 0))
}

const SuperRole = "super"
const AdminRole = "admin"

func (c UserClaims) IsSuper() bool {
	return c.HasRole(SuperRole)
}

func (c UserClaims) IsAdmin() bool {
	return c.IsSuper() || c.HasRole(AdminRole)
}

func (c UserClaims) HasRole(role string) bool {
	for _, item := range c.Roles {
		if item == role {
			return true
		}
	}

	return false
}

func (c UserClaims) AllowedToAccessClient(clientId primitive.ObjectID) bool {
	if c.IsSuper() {
		return true
	}

	for _, item := range c.Clients {
		if item.Id.Hex() == clientId.Hex() {
			return true
		}
	}

	return false
}
