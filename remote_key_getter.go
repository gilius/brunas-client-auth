package auth

import (
	"bitbucket.org/gilius/brunas-client-auth/models"
	"encoding/json"
	"log"
	"net/http"
	"time"
)

type remoteKeyUpdater struct {
	keysRepository KeyRepository
	logger         *log.Logger
}

const RsaPublicKeyFetchInterval = 24 * time.Hour

const ServiceUrl = "https://auth.brunas.lt"

func (s remoteKeyUpdater) Init() {
	ticker := time.NewTicker(RsaPublicKeyFetchInterval)
	done := make(chan bool)
	s.fetchPublicKey()

	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				s.fetchPublicKey()
			}
		}
	}()
}

func (s remoteKeyUpdater) fetchPublicKey() {
	s.logger.Println("Fetching public key")
	response, err := http.Get(ServiceUrl + "/keys")
	if err != nil {
		s.logger.Println("Failed to fetch Auth RSA public key")
		s.logger.Println(err)
		return
	}

	var item models.AuthRsaResponse
	err = json.NewDecoder(response.Body).Decode(&item)
	if err != nil {
		s.logger.Println("Failed to decode Auth RSA public key response")
		s.logger.Println(err)
		return
	}

	toolboxError := s.keysRepository.PutPublicKey(item.Data)
	if toolboxError != nil {
		s.logger.Println("Failed to save Auth RSA public key")
		s.logger.Println(toolboxError)
		return
	}
}

//goland:noinspection GoUnusedExportedFunction
func NewRemoteKeyUpdater(keysRepository KeyRepository, logger *log.Logger) KeyUpdater {
	return &remoteKeyUpdater{
		keysRepository: keysRepository,
		logger:         logger,
	}
}
