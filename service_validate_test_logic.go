package auth

import (
	"bitbucket.org/gilius/brunas-client-auth/models"
	"bitbucket.org/gilius/go-toolbox"
	"github.com/gorilla/websocket"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
	"net/http"
	"sync"
	"time"
)

type ValidateTestService struct {
	logger            *log.Logger
	usersClaimsList   map[string]models.UserClaims
	serviceClaimsList map[string]models.ServiceClaims
}

func (s ValidateTestService) VerifyUserClaims(tokenString string) (*models.UserClaims, *toolbox.Error) {

	var claims *models.UserClaims
	if val, ok := s.usersClaimsList[tokenString]; ok {
		claims = &val
	}

	if claims == nil {
		return nil, toolbox.ErrorUnauthorized(ErrorMissingJWT)
	}

	if claims.IsExpired() {
		return nil, toolbox.ErrorUnauthorized(ErrorMissingJWT)
	}

	return claims, nil
}

func (s ValidateTestService) SocketAnyClaimsRequest(_ func(*websocket.Conn, *http.Request, *sync.Mutex, *models.ServiceClaims, *models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	panic("implement me")
}

func (s ValidateTestService) VerifyServiceClaims(tokenString string) (*models.ServiceClaims, *toolbox.Error) {

	var claims *models.ServiceClaims
	if val, ok := s.serviceClaimsList[tokenString]; ok {
		claims = &val
	}

	if claims == nil {
		return nil, toolbox.ErrorUnauthorized(ErrorMissingJWT)
	}

	if !claims.Service {
		return nil, toolbox.ErrorUnauthorized(ErrorInvalidJWT)
	}

	if claims.IsExpired() {
		return nil, toolbox.ErrorUnauthorized(ErrorExpiredJWT)
	}

	return claims, nil
}

func (s ValidateTestService) ClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.Claims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, err := s.VerifyUserClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}
		if claims == nil {
			toolbox.HandleHttpError(toolbox.ErrorForbidden("no_access_x"))
		}
		handler(writer, request, models.Claims{
			ClientIdList: claims.ClientIdList,
			ExpiresAt:    claims.ExpiresAt,
			Claims:       claims.Claims,
		})
	})
}

func (s ValidateTestService) UserClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, err := s.VerifyUserClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}
		if claims == nil {
			toolbox.HandleHttpError(toolbox.ErrorForbidden("no_access_x"))
		}
		handler(writer, request, *claims)
	})
}

func (s ValidateTestService) ServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.ServiceClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, err := s.VerifyServiceClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}
		if claims == nil {
			toolbox.HandleHttpError(toolbox.ErrorForbidden("no_access_x"))
		}
		handler(writer, request, *claims)
	})
}

func (s ValidateTestService) SuperOrServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.VerifyUserClaims(jwt)

		if claims != nil && claims.IsSuper() {
			handler(writer, request, nil)
			return
		}

		serviceClaims, err := s.VerifyServiceClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}

		handler(writer, request, serviceClaims)
	})
}

func (s ValidateTestService) AnyClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims, *models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.VerifyUserClaims(jwt)

		if claims != nil {
			handler(writer, request, nil, claims)
			return
		}

		serviceClaims, err := s.VerifyServiceClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}

		handler(writer, request, serviceClaims, nil)
	})
}

func (s ValidateTestService) OptionalClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.Claims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.VerifyUserClaims(jwt)
		if claims == nil {
			handler(writer, request, nil)
			return
		}
		handler(writer, request, &models.Claims{
			ClientIdList: claims.ClientIdList,
			ExpiresAt:    claims.ExpiresAt,
			Claims:       claims.Claims,
		})
	})
}

func (s ValidateTestService) OptionalUserClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.VerifyUserClaims(jwt)
		handler(writer, request, claims)
	})
}

func (s ValidateTestService) OptionalServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.VerifyServiceClaims(jwt)
		handler(writer, request, claims)
	})
}

func (s ValidateTestService) SocketUserJWTClaimsRequest(_ func(*websocket.Conn, *http.Request, *sync.Mutex, models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	panic("implement me")
}

func (s ValidateTestService) SocketJWTClaimsRequest(_ func(*websocket.Conn, *http.Request, *sync.Mutex, models.Claims)) func(http.ResponseWriter, *http.Request) {
	panic("implement me")
}

const TestSimpleJWT = "jwt_simple"
const TestAdminJWT = "jwt_admin"
const TestSuperJWT = "jwt_super"

const TestServiceClientJwt = "jwt_service_client"
const TestServiceJwt = "jwt_service"

func (s ValidateTestService) addTestClaims() {

	id, _ := primitive.ObjectIDFromHex("655e1d82617f087856a1f616") //This is hardcoded, so it's always the same when server and client creates validation services

	client := models.Client{
		Id:      id,
		Name:    "Test1",
		Domains: []string{"test.test.lt"},
	}

	//JWT service client
	s.serviceClaimsList[TestServiceClientJwt] = models.ServiceClaims{
		Id:           primitive.NewObjectID(),
		Service:      true,
		ClientIdList: []primitive.ObjectID{client.Id},
		ExpiresAt:    time.Now().Add(time.Hour * 24).Unix(),
	}

	//JWT service
	s.serviceClaimsList[TestServiceJwt] = models.ServiceClaims{
		Id:           primitive.NewObjectID(),
		Service:      true,
		ClientIdList: []primitive.ObjectID{},
		ExpiresAt:    time.Now().Add(time.Hour * 24).Unix(),
	}

	//JWT simple
	s.usersClaimsList[TestSimpleJWT] = models.UserClaims{
		Id:           primitive.NewObjectID(),
		FirstName:    "Test1",
		LastName:     "Tester1",
		Email:        "test1@test.lt",
		Clients:      []models.Client{client},
		ClientIdList: []primitive.ObjectID{client.Id},
		ExpiresAt:    time.Now().Add(time.Hour * 24).Unix(),
	}

	//JWT admin
	s.usersClaimsList[TestAdminJWT] = models.UserClaims{
		Id:        primitive.NewObjectID(),
		FirstName: "Test2",
		LastName:  "Tester2",
		Email:     "test2@test.lt",
		Roles:     []string{models.AdminRole},
		Clients:   []models.Client{client},
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
	}

	//JWT super
	s.usersClaimsList[TestSuperJWT] = models.UserClaims{
		Id:        primitive.NewObjectID(),
		FirstName: "Test2",
		LastName:  "Tester2",
		Email:     "test2@test.lt",
		Roles:     []string{models.SuperRole},
		Clients:   []models.Client{client},
		ExpiresAt: time.Now().Add(time.Hour * 24).Unix(),
	}
}

//goland:noinspection GoUnusedExportedFunction
func NewValidateTestService(logger *log.Logger) *ValidateTestService {
	service := &ValidateTestService{
		logger:            logger,
		usersClaimsList:   map[string]models.UserClaims{},
		serviceClaimsList: map[string]models.ServiceClaims{},
	}
	service.addTestClaims()

	return service
}
