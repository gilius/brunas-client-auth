package auth

import (
	"bitbucket.org/gilius/go-toolbox"
	"crypto/rsa"
)

type KeyProvider interface {
	GetPublicKey() (*rsa.PublicKey, *toolbox.Error)
}
