package auth

import (
	"bitbucket.org/gilius/brunas-client-auth/models"
	"bitbucket.org/gilius/go-toolbox"
	"encoding/json"
	"github.com/golang-jwt/jwt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"strings"
	"sync"
)

type validateService struct {
	keysRepository KeyProvider
	logger         *log.Logger
}

func (s validateService) decodeClaims(tokenString string) ([]byte, *toolbox.Error) {
	///Unauthorized
	if tokenString == "" {
		return nil, toolbox.ErrorUnauthorized(ErrorMissingJWT)
	}

	VerifyKey, keyErr := s.keysRepository.GetPublicKey()
	if keyErr != nil {
		s.logger.Println(keyErr)
		return nil, toolbox.ErrorInternal(ErrorCantGetPublicKey)
	}

	if VerifyKey == nil {
		s.logger.Println("Failed to create key?")
		return nil, toolbox.ErrorInternal(ErrorCantGetPublicKey)
	}

	verifiedToken, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return VerifyKey, nil
	})
	if err != nil {
		return nil, toolbox.ErrorUnauthorized(ErrorInvalidJWT)
	}

	body := strings.Split(verifiedToken.Raw, ".")[1]
	decoded, err := jwt.DecodeSegment(body)
	if err != nil {
		return nil, toolbox.ErrorUnauthorized(ErrorInvalidJWT)
	}
	return decoded, nil
}

func (s validateService) VerifyUserClaims(tokenString string) (*models.UserClaims, *toolbox.Error) {
	decoded, errDecode := s.decodeClaims(tokenString)
	if errDecode != nil {
		return nil, errDecode
	}

	var claims *models.UserClaims
	err := json.Unmarshal(decoded, &claims)
	if err != nil {
		return nil, toolbox.ErrorUnauthorized(ErrorInvalidJWT)
	}

	if claims.IsExpired() {
		return nil, toolbox.ErrorUnauthorized(ErrorExpiredJWT)
	}

	return claims, nil
}

func (s validateService) VerifyServiceClaims(tokenString string) (*models.ServiceClaims, *toolbox.Error) {
	decoded, errDecode := s.decodeClaims(tokenString)
	if errDecode != nil {
		return nil, errDecode
	}

	var claims *models.ServiceClaims
	err := json.Unmarshal(decoded, &claims)
	if err != nil {
		return nil, toolbox.ErrorUnauthorized(ErrorInvalidJWT)
	}

	if !claims.Service {
		return nil, toolbox.ErrorUnauthorized(ErrorInvalidJWT)
	}

	if claims.IsExpired() {
		return nil, toolbox.ErrorUnauthorized(ErrorExpiredJWT)
	}

	return claims, nil
}

func (s validateService) verifyClaims(tokenString string) (*models.Claims, *toolbox.Error) {
	decoded, errDecode := s.decodeClaims(tokenString)
	if errDecode != nil {
		return nil, errDecode
	}

	var claims *models.Claims
	err := json.Unmarshal(decoded, &claims)
	if err != nil {
		return nil, toolbox.ErrorUnauthorized(ErrorInvalidJWT)
	}

	if claims.IsExpired() {
		return nil, toolbox.ErrorUnauthorized(ErrorExpiredJWT)
	}

	return claims, nil
}

func (s validateService) UserClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, err := s.VerifyUserClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}
		handler(writer, request, *claims)
	})
}

func (s validateService) ClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.Claims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, err := s.verifyClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}
		handler(writer, request, *claims)
	})
}

func (s validateService) ServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, models.ServiceClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, err := s.VerifyServiceClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}
		handler(writer, request, *claims)
	})
}

func (s validateService) SuperOrServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.VerifyUserClaims(jwt)

		if claims != nil && claims.IsSuper() {
			handler(writer, request, nil)
			return
		}

		serviceClaims, err := s.VerifyServiceClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}

		handler(writer, request, serviceClaims)
	})
}

func (s validateService) AnyClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims, *models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		serviceClaims, _ := s.VerifyServiceClaims(jwt)

		if serviceClaims != nil {
			handler(writer, request, serviceClaims, nil)
			return
		}

		claims, err := s.VerifyUserClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}
		if claims != nil {
			handler(writer, request, nil, claims)
		}
	})
}

func (s validateService) SocketAnyClaimsRequest(handler func(*websocket.Conn, *http.Request, *sync.Mutex, *models.ServiceClaims, *models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.SocketConnectionBearerRequest(func(w *websocket.Conn, request *http.Request, writeLock *sync.Mutex, jwt string) {
		serviceClaims, _ := s.VerifyServiceClaims(jwt)

		if serviceClaims != nil {
			handler(w, request, writeLock, serviceClaims, nil)
			return
		}

		claims, err := s.VerifyUserClaims(jwt)
		if err != nil {
			toolbox.HandleHttpError(err)
		}
		if claims != nil {
			handler(w, request, writeLock, nil, claims)
		}
	})
}

func (s validateService) SocketJWTClaimsRequest(handler func(*websocket.Conn, *http.Request, *sync.Mutex, models.Claims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.SocketConnectionBearerRequest(func(w *websocket.Conn, request *http.Request, writeLock *sync.Mutex, jwt string) {
		claims, err := s.verifyClaims(jwt)
		if err != nil {
			return
		}
		handler(w, request, writeLock, *claims)
	})
}

func (s validateService) SocketUserJWTClaimsRequest(handler func(*websocket.Conn, *http.Request, *sync.Mutex, models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.SocketConnectionBearerRequest(func(w *websocket.Conn, request *http.Request, writeLock *sync.Mutex, jwt string) {
		claims, err := s.VerifyUserClaims(jwt)
		if err != nil {
			return
		}
		handler(w, request, writeLock, *claims)
	})
}

func (s validateService) OptionalClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.Claims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.verifyClaims(jwt)
		handler(writer, request, claims)
	})
}
func (s validateService) OptionalUserClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.UserClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.VerifyUserClaims(jwt)
		handler(writer, request, claims)
	})
}
func (s validateService) OptionalServiceClaimsRequest(handler func(http.ResponseWriter, *http.Request, *models.ServiceClaims)) func(http.ResponseWriter, *http.Request) {
	return toolbox.BearerAuthRequest(func(writer http.ResponseWriter, request *http.Request, jwt string) {
		claims, _ := s.VerifyServiceClaims(jwt)
		handler(writer, request, claims)
	})
}

//goland:noinspection GoUnusedExportedFunction
func NewValidateService(keysRepository KeyProvider, logger *log.Logger, fetcher KeyUpdater) ServiceValidate {
	if fetcher != nil {
		fetcher.Init()
	}
	return &validateService{
		keysRepository: keysRepository,
		logger:         logger,
	}
}
