package models

import (
	"github.com/golang-jwt/jwt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ServiceClaims struct {
	Id           primitive.ObjectID   `json:"id"`
	Service      bool                 `json:"service"`
	ClientIdList []primitive.ObjectID `json:"clientIdList"`
	ExpiresAt    int64                `json:"exp,omitempty"`
	jwt.Claims   `json:"-"`
}

func (c ServiceClaims) IsExpired() bool {
	return time.Now().After(time.Unix(c.ExpiresAt, 0))
}

func (c ServiceClaims) CanAccessClient(clientId *primitive.ObjectID) bool {
	if len(c.ClientIdList) == 0 {
		return true
	}
	if clientId == nil {
		return false
	}
	for _, client := range c.ClientIdList {
		if client.Hex() == clientId.Hex() {
			return true
		}
	}
	return false
}
