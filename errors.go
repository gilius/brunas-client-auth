package auth

//goland:noinspection GoUnusedConst
const ErrorForbidden = "forbidden"
const ErrorInvalidJWT = "invalid_jwt"
const ErrorCantGetPublicKey = "cannot_fetch_public_key"
const ErrorMissingJWT = "missing_jwt"
const ErrorExpiredJWT = "jwt_expired"
